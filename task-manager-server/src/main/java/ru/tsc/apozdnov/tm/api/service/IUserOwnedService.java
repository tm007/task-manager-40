package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.apozdnov.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

}
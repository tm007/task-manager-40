package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    void create(@NotNull String userId, @NotNull String name);

    void create(@NotNull String userId, @NotNull String name, @NotNull String description);

    void create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    Project updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    Project changeProjectStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

}
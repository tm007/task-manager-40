package ru.tsc.apozdnov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.SortSpecification;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.api.service.IConnectionService;
import ru.tsc.apozdnov.tm.api.service.IProjectService;
import ru.tsc.apozdnov.tm.comparator.CreatedComparator;
import ru.tsc.apozdnov.tm.comparator.DateBeginComparator;
import ru.tsc.apozdnov.tm.comparator.StatusComparator;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exception.field.EmptyDescriptionException;
import ru.tsc.apozdnov.tm.exception.field.EmptyIdException;
import ru.tsc.apozdnov.tm.exception.field.EmptyNameException;
import ru.tsc.apozdnov.tm.exception.field.EmptyUserIdException;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.model.ProjectProvider;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static org.mybatis.dynamic.sql.SqlBuilder.select;
import static ru.tsc.apozdnov.tm.model.ProjectProvider.*;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected SortSpecification getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return created;
        else if (comparator == StatusComparator.INSTANCE) return status;
        else if (comparator == DateBeginComparator.INSTANCE) return dateBegin;
        else return name;
    }

    @Override
    public void add(@NotNull final Project model) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final Project model) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            model.setUserId(userId);
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    public Collection<Project> set(@NotNull final Collection<Project> models) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clear();
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return models;
    }

    @Override
    @SneakyThrows
    public void create(@NotNull final String userId, @NotNull final String name) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        @NotNull Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clearByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Project project = findOneById(userId, id);
        project.setStatus(status);
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAllByUserId(userId);
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull String userId, @Nullable Sort sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Override
    public @NotNull List<Project> findAll(@NotNull String userId, @NotNull Comparator<Project> comparator) {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final SelectStatementProvider selectStatementProvider =
                    select(id, ProjectProvider.userId, name, description, status, created, dateBegin, dateEnd)
                            .from(project)
                            .where(ProjectProvider.userId, isEqualTo(userId))
                            .orderBy(getSortType(comparator))
                            .build()
                            .render(RenderingStrategies.MYBATIS3);
            return repository.findAllComparatorByUserId(selectStatementProvider);
        }
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findOneByIdAndUserId(userId, id);
        }
    }

    @Override
    public boolean existsById(@NotNull String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.existsById(id);
        }
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.existsByIdAndUserId(userId, id);
        }
    }

    @Override
    public void remove(@NotNull Project model) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.remove(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@NotNull String userId, @NotNull Project model) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.remove(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(@NotNull String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Project project;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneById(id);
            if (project == null) return;
            repository.remove(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = getSession();
        @Nullable final Project project;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneByIdAndUserId(userId, id);
            if (project == null) return;
            repository.remove(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void update(@NotNull Project model) {
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public long getCount() {
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.getCount();
        }
    }

    @Override
    public long getCount(@NotNull String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final SqlSession sqlSession = getSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.getCountByUserId(userId);
        }
    }

}
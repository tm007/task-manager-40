package ru.tsc.apozdnov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Insert("INSERT INTO tm_task (id, created, name, description, status, user_id, project_id, date_begin, date_end)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId}, #{dateBegin}, #{dateEnd})")
    void add(@NotNull Task task);

    @Insert("INSERT INTO tm_task (id, created, name, description, status, user_id, project_id, date_begin, date_end)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId}, #{dateBegin}, #{dateEnd})")
    void addAll(@NotNull Collection<Task> tasks);

    @Delete("DELETE FROM tm_task")
    void clear();

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clearByUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Task> findAll();

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM tm_task "
            + "WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Task> findAllByUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    List<Task> findAllComparator(SelectStatementProvider selectStatementProvider);

    @Select("SELECT count(1) = 1 FROM tm_task WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull String id);

    @Select("SELECT count(1) = 1 FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    boolean existsByIdAndUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM tm_task "
            + "WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Task findOneById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM tm_task "
            + "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    Task findOneByIdAndUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void remove(@NotNull Task task);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void removeAll(@Nullable Collection<Task> collection);

    @Update("UPDATE tm_task SET created = #{created}, name = #{name}, description = #{description}, "
            + "status = #{status}, project_id = #{projectId}, date_begin = #{dateBegin}, date_end = #{dateEnd} "
            + "WHERE id = #{id}")
    void update(@NotNull Task task);

    @Select("SELECT count(1) FROM tm_task")
    long getCount();

    @Select("SELECT count(1) FROM tm_task WHERE user_id = #{userId}")
    long getCountByUserId(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end FROM tm_task "
            + "WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id,"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    List<Task> findAllByProjectId(@Param("userId") @Nullable String userId, @Param("projectId") @Nullable String projectId);

    @Delete("DELETE FROM tm_task WHERE project_id = #{projectId}")
    void removeTasksByProjectId(@Param("projectId") @NotNull String projectId);

}
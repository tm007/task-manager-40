package ru.tsc.apozdnov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.repository.IRepository;
import ru.tsc.apozdnov.tm.api.service.IConnectionService;
import ru.tsc.apozdnov.tm.api.service.IService;
import ru.tsc.apozdnov.tm.model.AbstractModel;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected SqlSession getSession() {
        return connectionService.getSqlSession();
    }

}
//package ru.tsc.apozdnov.tm.repository;
//
//import org.jetbrains.annotations.NotNull;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
//import ru.tsc.apozdnov.tm.model.Project;
//
//import java.util.List;
//import java.util.UUID;
//
//public class ProjectRepositoryTest {
//
//    @NotNull
//    private static final String USERID_CREATE = UUID.randomUUID().toString();
//
//    @NotNull
//    private static final String USERID_CREATE_2 = UUID.randomUUID().toString();
//
//    private static long INITIAL_SIZE_USER;
//
//    @NotNull
//    private final IProjectRepository projectRepository = new ProjectRepository();
//
//    @Before
//    public void init() {
//        projectRepository.create(USERID_CREATE, "test-1");
//        projectRepository.create(USERID_CREATE, "test-2");
//        projectRepository.create(USERID_CREATE_2, "test-3");
//        INITIAL_SIZE_USER = projectRepository.getSize();
//    }
//
//    @Test
//    public void create() {
//        projectRepository.create(USERID_CREATE, "test");
//        Assert.assertEquals(INITIAL_SIZE_USER + 1, projectRepository.getSize());
//        projectRepository.create(USERID_CREATE, "test", "description");
//        Assert.assertEquals(INITIAL_SIZE_USER + 2, projectRepository.getSize());
//    }
//
//    @Test
//    public void add() {
//        @NotNull final Project project = new Project();
//        projectRepository.add(USERID_CREATE, project);
//        Assert.assertEquals(INITIAL_SIZE_USER + 1, projectRepository.getSize());
//        Assert.assertTrue(projectRepository.findAll().contains(project));
//    }
//
//    @Test
//    public void clear() {
//        projectRepository.clear();
//        Assert.assertEquals(0, projectRepository.getSize());
//    }
//
//    @Test
//    public void findAll() {
//        @NotNull final List<Project> projectsAll = projectRepository.findAll();
//        Assert.assertEquals(INITIAL_SIZE_USER, projectsAll.size());
//        @NotNull final List<Project> projectsOwnedUser1 = projectRepository.findAll(USERID_CREATE);
//        Assert.assertEquals(2, projectsOwnedUser1.size());
//        @NotNull final List<Project> projectsOwnedUser3 = projectRepository.findAll(UUID.randomUUID().toString());
//        Assert.assertEquals(0, projectsOwnedUser3.size());
//    }
//
//    @Test
//    public void findOneById() {
//        @NotNull final String projectName = "test find by id";
//        @NotNull final Project project = projectRepository.create(USERID_CREATE, projectName);
//        @NotNull final String projectId = project.getId();
//        Assert.assertNotNull(projectRepository.findOneById(projectId));
//        Assert.assertEquals(projectName, projectRepository.findOneById(projectId).getName());
//        Assert.assertNull(projectRepository.findOneById(UUID.randomUUID().toString()));
//        Assert.assertNotNull(projectRepository.findOneById(USERID_CREATE, projectId));
//        Assert.assertEquals(projectName, projectRepository.findOneById(USERID_CREATE, projectId).getName());
//        Assert.assertNull(projectRepository.findOneById(USERID_CREATE, UUID.randomUUID().toString()));
//    }
//
//    @Test
//    public void findOneByIndex() {
//        @NotNull final String projectName = "test find by index";
//        projectRepository.create(USERID_CREATE, projectName);
//        @NotNull final Integer projectIndex = 2;
//        Assert.assertNotNull(projectRepository.findOneByIndex(USERID_CREATE, projectIndex));
//        Assert.assertEquals(projectName, projectRepository.findOneByIndex(USERID_CREATE, projectIndex).getName());
//    }
//
//    @Test
//    public void existsById() {
//        @NotNull final String projectName = "test exist by id";
//        @NotNull final Project project = projectRepository.create(USERID_CREATE, projectName);
//        @NotNull final String projectId = project.getId();
//        Assert.assertTrue(projectRepository.existsById(projectId));
//        Assert.assertFalse(projectRepository.existsById(UUID.randomUUID().toString()));
//    }
//
//    @Test
//    public void remove() {
//        @NotNull final Project project = projectRepository.create(USERID_CREATE, "test");
//        @NotNull final String projectId = project.getId();
//        projectRepository.remove(project);
//        Assert.assertNull(projectRepository.findOneById(projectId));
//        Assert.assertEquals(INITIAL_SIZE_USER, projectRepository.getSize());
//        projectRepository.add(project);
//        projectRepository.remove(USERID_CREATE, project);
//        Assert.assertNull(projectRepository.findOneById(USERID_CREATE, projectId));
//        Assert.assertEquals(INITIAL_SIZE_USER, projectRepository.getSize());
//    }
//
//    @Test
//    public void removeById() {
//        @NotNull final Project project = projectRepository.create(USERID_CREATE, "test");
//        @NotNull final String projectId = project.getId();
//        projectRepository.removeById(projectId);
//        Assert.assertNull(projectRepository.findOneById(projectId));
//        Assert.assertEquals(INITIAL_SIZE_USER, projectRepository.getSize());
//        projectRepository.add(project);
//        projectRepository.removeById(USERID_CREATE, projectId);
//        Assert.assertNull(projectRepository.findOneById(USERID_CREATE, projectId));
//        Assert.assertEquals(INITIAL_SIZE_USER, projectRepository.getSize());
//    }
//
//    @Test
//    public void removeByIndex() {
//        projectRepository.create(USERID_CREATE, "test");
//        @NotNull final Integer projectIndex = 2;
//        projectRepository.removeByIndex(USERID_CREATE, projectIndex);
//        Assert.assertThrows(IndexOutOfBoundsException.class,
//                () -> projectRepository.findOneByIndex(USERID_CREATE, projectIndex));
//        Assert.assertEquals(INITIAL_SIZE_USER, projectRepository.getSize());
//    }
//
//}
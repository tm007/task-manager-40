package ru.tsc.apozdnov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class UserLogoutRequest extends AbstractUserRequest {

    public UserLogoutRequest(@Nullable final String token) {
        super(token);
    }

}
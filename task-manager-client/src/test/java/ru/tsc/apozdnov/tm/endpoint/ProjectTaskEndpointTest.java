//package ru.tsc.apozdnov.tm.endpoint;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//import ru.tsc.apozdnov.tm.api.endpoint.IAuthEndpoint;
//import ru.tsc.apozdnov.tm.api.endpoint.IProjectEndpoint;
//import ru.tsc.apozdnov.tm.api.endpoint.IProjectTaskEndpoint;
//import ru.tsc.apozdnov.tm.api.endpoint.ITaskEndpoint;
//import ru.tsc.apozdnov.tm.api.service.IPropertyService;
//import ru.tsc.apozdnov.tm.dto.request.*;
//import ru.tsc.apozdnov.tm.dto.response.ProjectCreateResponse;
//import ru.tsc.apozdnov.tm.dto.response.TaskCreateResponse;
//import ru.tsc.apozdnov.tm.dto.response.TaskShowByIdResponse;
//import ru.tsc.apozdnov.tm.dto.response.UserLoginResponse;
//import ru.tsc.apozdnov.tm.marker.ISoapCategory;
//import ru.tsc.apozdnov.tm.model.Project;
//import ru.tsc.apozdnov.tm.model.Task;
//import ru.tsc.apozdnov.tm.service.PropertyService;
//
//@Category(ISoapCategory.class)
//public final class ProjectTaskEndpointTest {
//
//    @NotNull
//    private final IPropertyService propertyService = new PropertyService();
//
//    @NotNull
//    private final String host = propertyService.getServerHost();
//
//    @NotNull
//    private final String port = Integer.toString(propertyService.getServerPort());
//
//    @NotNull
//    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);
//
//    @NotNull
//    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);
//
//    @NotNull
//    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);
//
//    @NotNull
//    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance(host, port);
//
//    @Nullable
//    private String token;
//
//    @Nullable
//    private Project projectBefore;
//
//    @Nullable
//    private Task taskBefore;
//
//    @Before
//    public void init() {
//        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("user", "user"));
//        token = loginResponse.getToken();
//        projectEndpoint.clearProject(new ProjectClearRequest(token));
//        @NotNull final ProjectCreateResponse createProjectResponse = projectEndpoint.createProject(
//                new ProjectCreateRequest(token, "test", "test", null, null)
//        );
//        projectBefore = createProjectResponse.getProject();
//        taskEndpoint.clearTask(new TaskClearRequest(token));
//        @NotNull final TaskCreateResponse createTaskResponse = taskEndpoint.createTask(
//                new TaskCreateRequest(token, "test", "test", null, null)
//        );
//        taskBefore = createTaskResponse.getTask();
//    }
//
//    @Test
//    public void bindTaskToProject() {
//        Assert.assertThrows(Exception.class,
//                () -> projectTaskEndpoint.bindTaskToProject(
//                        new TaskBindToProjectRequest(null, taskBefore.getId(), projectBefore.getId())));
//        projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, taskBefore.getId(), projectBefore.getId()));
//        @NotNull final TaskShowByIdResponse response = taskEndpoint.showTaskById(
//                new TaskShowByIdRequest(token, taskBefore.getId()));
//        Assert.assertNotNull(response.getTask().getProjectId());
//    }
//
//    @Test
//    public void unbindTaskFromProject() {
//        projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, taskBefore.getId(), projectBefore.getId()));
//        @NotNull final TaskShowByIdResponse response = taskEndpoint.showTaskById(
//                new TaskShowByIdRequest(token, taskBefore.getId()));
//        Assert.assertNotNull(response.getTask().getProjectId());
//        projectTaskEndpoint.unbindTaskFromProject(
//                new TaskUnbindFromProjectRequest(token, taskBefore.getId(), projectBefore.getId()));
//        @NotNull final TaskShowByIdResponse responseAfterUnbind = taskEndpoint.showTaskById(
//                new TaskShowByIdRequest(token, taskBefore.getId()));
//        Assert.assertNull(responseAfterUnbind.getTask().getProjectId());
//    }
//
//}
//package ru.tsc.apozdnov.tm.endpoint;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//import ru.tsc.apozdnov.tm.api.endpoint.IAuthEndpoint;
//import ru.tsc.apozdnov.tm.api.endpoint.IProjectEndpoint;
//import ru.tsc.apozdnov.tm.api.service.IPropertyService;
//import ru.tsc.apozdnov.tm.dto.request.*;
//import ru.tsc.apozdnov.tm.dto.response.*;
//import ru.tsc.apozdnov.tm.enumerated.Status;
//import ru.tsc.apozdnov.tm.marker.ISoapCategory;
//import ru.tsc.apozdnov.tm.model.Project;
//import ru.tsc.apozdnov.tm.service.PropertyService;
//import ru.tsc.apozdnov.tm.util.DateUtil;
//
//import java.util.Date;
//
//@Category(ISoapCategory.class)
//public final class ProjectEndpointTest {
//
//    @NotNull
//    private final IPropertyService propertyService = new PropertyService();
//
//    @NotNull
//    private final String host = propertyService.getServerHost();
//
//    @NotNull
//    private final String port = Integer.toString(propertyService.getServerPort());
//
//    @NotNull
//    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);
//
//    @NotNull
//    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);
//
//    @Nullable
//    private String token;
//
//    @Nullable
//    private Project projectBefore;
//
//    @Before
//    public void init() {
//        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("user", "user"));
//        token = loginResponse.getToken();
//        projectEndpoint.clearProject(new ProjectClearRequest(token));
//        @NotNull final ProjectCreateResponse createResponse = projectEndpoint.createProject(
//                new ProjectCreateRequest(token, "test", "test", null, null)
//        );
//        projectBefore = createResponse.getProject();
//    }
//
//    @Test
//    public void changeProjectStatusById() {
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.changeProjectStatusById(
//                        new ProjectChangeStatusByIdRequest(null, projectBefore.getId(), Status.IN_PROGRESS)));
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.changeProjectStatusById(
//                        new ProjectChangeStatusByIdRequest(token, projectBefore.getId(), null)));
//        ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(
//                new ProjectChangeStatusByIdRequest(token, projectBefore.getId(), Status.IN_PROGRESS));
//        Assert.assertNotNull(response);
//        @Nullable Project project = response.getProject();
//        Assert.assertNotEquals(projectBefore.getStatus(), project.getStatus());
//    }
//
//    @Test
//    public void createProject() {
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.createProject(
//                        new ProjectCreateRequest(null, "", "", null, null)));
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.createProject(
//                        new ProjectCreateRequest(token, "", "", null, null)));
//        @NotNull final String projectName = "name";
//        @NotNull final Date dateBegin = DateUtil.toDate("10.10.2021");
//        @NotNull final Date dateEnd = DateUtil.toDate("11.11.2021");
//        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
//                new ProjectCreateRequest(token, "name", "description", dateBegin, dateEnd));
//        Assert.assertNotNull(response);
//        @Nullable Project project = response.getProject();
//        Assert.assertEquals(projectName, project.getName());
//    }
//
//    @Test
//    public void removeProjectByIdTest() {
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(null, projectBefore.getId())));
//        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, projectBefore.getId()));
//        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(token, null)).getProjects());
//    }
//
//    @Test
//    public void removeProjectByIndexTest() {
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(null, 0)));
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(token, -1)));
//        projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(token, 0));
//        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(token, null)).getProjects());
//    }
//
//    @Test
//    public void showProjectById() {
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.showProjectById(new ProjectShowByIdRequest(null, projectBefore.getId())));
//        @NotNull final ProjectShowByIdResponse response = projectEndpoint.showProjectById(
//                new ProjectShowByIdRequest(token, projectBefore.getId()));
//        Assert.assertNotNull(response);
//        Assert.assertNotNull(response.getProject());
//    }
//
//    @Test
//    public void showProjectByIndex() {
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.showProjectByIndex(new ProjectShowByIndexRequest(null, 0)));
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.showProjectByIndex(new ProjectShowByIndexRequest(token, -1)));
//        @NotNull final ProjectShowByIndexResponse response = projectEndpoint.showProjectByIndex(
//                new ProjectShowByIndexRequest(token, 0));
//        Assert.assertNotNull(response);
//        Assert.assertNotNull(response.getProject());
//    }
//
//    @Test
//    public void updateProjectById() {
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.updateProjectById(
//                        new ProjectUpdateByIdRequest(null, projectBefore.getId(), "", "")));
//        @NotNull final ProjectUpdateByIdResponse response = projectEndpoint.updateProjectById(
//                new ProjectUpdateByIdRequest(token, projectBefore.getId(), "new_name", "new_description"));
//        Assert.assertNotNull(response);
//        @Nullable Project project = response.getProject();
//        Assert.assertNotNull(project);
//        Assert.assertNotEquals(projectBefore.getName(), project.getName());
//    }
//
//    @Test
//    public void updateProjectByIndex() {
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.updateProjectByIndex(
//                        new ProjectUpdateByIndexRequest(null, 0, "", "")));
//        Assert.assertThrows(Exception.class,
//                () -> projectEndpoint.updateProjectByIndex(
//                        new ProjectUpdateByIndexRequest(token, -1, "", "")));
//        @NotNull final ProjectUpdateByIndexResponse response = projectEndpoint.updateProjectByIndex(
//                new ProjectUpdateByIndexRequest(token, 0, "new_name", "new_description"));
//        Assert.assertNotNull(response);
//        @Nullable Project project = response.getProject();
//        Assert.assertNotNull(project);
//        Assert.assertNotEquals(projectBefore.getName(), project.getName());
//    }
//
//}